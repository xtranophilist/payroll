#include <stdio.h>
#include <conio.h>
#include <string.h>
#include <stdlib.h>
#include <ctype.h>



void splashScreen();
void makeBox(int, int, int, int);
void printArt();
void echoContinue();
void displayMainMenu();
void echoHead();
void displayLogin();
int scanInt();
void scanPassword(char *);
void scanString(char *);
void showMessage(char *);
void showError(char *);
void manageEmployees();
void manageDesignations();
void generateReport();
void doExit();
void listEmployees();
void searchEmployee();
void addEmployee();
void editEmployee();
void removeEmployee();
void listDesignations();
void addDesignation();
void editDesignation();
void removeDesignation();
int getNextDesignationID();
int getNextEmployeeID();
float getSalaryByDesignation(int);
void getDesignationByID(char *,int);
void searchByID();
void searchByName();
void searchByDesignation();
void searchBySalary();
void convertToLower(char *);

struct employee{
	int id;
	char fullName[50];
	int designationID;
	}theEmployee;

struct designation{
	int id;
	char designation[30];
	float salary;
	char description[100];
	}theDesignation;


float getSalaryByDesignation(int id){
FILE *fp;
long int size=sizeof(theDesignation);
fp=fopen("des.dat","rb+");
if (fp==NULL){ // if the file couldn't be read
      showError("Couldn't open designation file for getting salary!");
	    }
	    else{
while(fread(&theDesignation,size,1,fp))
{
if(theDesignation.id==id){
fclose(fp);
return theDesignation.salary;
}
}
showError("Illegal ID for Designation!");
}
getch();
fclose(fp);
return 0;
}


void getDesignationByID(char *designation, int id){
FILE *fp;
long int size=sizeof(theDesignation);
int flg=0;
fp=fopen("des.dat","rb+");
if (fp==NULL){ // if the file couldn't be read
      showError("Couldn't open designation file for getting salary!");
	    }
	    else{
while(fread(&theDesignation,size,1,fp))
{
if(theDesignation.id==id){
strcpy(designation,theDesignation.designation);
flg=1;
break;
}
}
if(!flg){
showError("Illegal ID for Designation!");
getch();
}

}
fclose(fp);
}

int getNextDesignationID(){
FILE *fp;
int id;
long int size=sizeof(theDesignation);
fp=fopen("des.dat","rb+");
if (fp==NULL){ // if the file couldn't be read
      fclose(fp);
      return 1001;
      }
while(fread(&theDesignation,size,1,fp))//untill the end of the file
      id=theDesignation.id;
      fclose(fp);
      if (id<0) return 1001;
return (id+1);
}

int getNextEmployeeID(){
FILE *fp;
int id;
long int size=sizeof(theEmployee);
fp=fopen("emp.dat","rb+");
if (fp==NULL){ // if the file couldn't be read
	fclose(fp);
	return 1001;
	}
while(fread(&theEmployee,size,1,fp))//untill the end of the file
     id=theEmployee.id;
     fclose(fp);
     if (id<0) return 1001;
return (id+1);
}


void main(){
clrscr();
//splashScreen();
//displayLogin();
displayMainMenu();
getch();
}

void searchByID(){
clrscr();
echoHead();
int id, cnt=0,y=10;
char message[100];
char designation[50];
FILE *fp;
gotoxy(3,5);
cprintf("Enter ID of the employee to Search For:");
gotoxy(3,6);
id=scanInt();

clrscr();
echoHead();

fp=fopen("emp.dat","rb+");
rewind(fp);
if(fp==NULL){//if the file cudnt be opened
	showError("Error opening file");
	}    else{

while(fread(&theEmployee,sizeof(theEmployee),1,fp))
{
if(theEmployee.id==id){
cnt++;

gotoxy(2,7);
cprintf("ID");
gotoxy(15,7);
cprintf("Full Name");
gotoxy(35,7);
cprintf("Designation");
gotoxy(55,7);
cprintf("Basic Salary");

gotoxy(2,y);
cprintf("%d",theEmployee.id);
gotoxy(15,y);
cprintf("%s",theEmployee.fullName);
gotoxy(35,y);
getDesignationByID(designation,theEmployee.designationID);
cprintf("%s",designation);
gotoxy(55,y);
cprintf("%.2f",getSalaryByDesignation(theEmployee.designationID));
y++;
}
}
if(!cnt) showError("No employee exists with the provided ID!");
else{
sprintf(message,"%d",cnt);
strcat(message," Record(s) found ");
showMessage( message);
}

}
fclose(fp);
getch();
searchEmployee();
}

void searchByName(){
clrscr();
echoHead();
int cnt=0;
int flg=0,y=10;
char name[50];
char message[100];
char designation[50];
char *inputParts;
char *nameParts;
char *tmpName;
FILE *fp;
gotoxy(3,5);
cprintf("Enter Name of the employee to Search For:");
gotoxy(3,6);
scanString(name);

clrscr();
echoHead();

fp=fopen("emp.dat","rb+");
rewind(fp);
if(fp==NULL){//if the file cudnt be opened
	showError("Error opening file");
	}    else{

while(fread(&theEmployee,sizeof(theEmployee),1,fp))
{
//unset the flag
flg=0;
strcpy(tmpName,theEmployee.fullName);
//check against the whole name
if(!strcmpi(name,theEmployee.fullName)) flg=1;

//split and check for first name and last name
inputParts = strtok(name," ");
while(inputParts!=NULL)
{

nameParts = strtok(theEmployee.fullName," ");
while(nameParts!=NULL)
{
if(!strcmpi(inputParts,nameParts)) flg=1;
nameParts=strtok(NULL," ,");
}
inputParts=strtok(NULL," ,");

}
if(flg){
cnt++;

gotoxy(2,7);
cprintf("ID");
gotoxy(15,7);
cprintf("Full Name");
gotoxy(35,7);
cprintf("Designation");
gotoxy(55,7);
cprintf("Basic Salary");

gotoxy(2,y);
cprintf("%d",theEmployee.id);
gotoxy(15,y);
cprintf("%s",tmpName);
gotoxy(35,y);
getDesignationByID(designation,theEmployee.designationID);
cprintf("%s",designation);
gotoxy(55,y);
cprintf("%.2f",getSalaryByDesignation(theEmployee.designationID));
y++;
}
}
if(!cnt) showError("No employee exists with the provided name!");
else{
sprintf(message,"%d",cnt);
strcat(message," Record(s) found ");
showMessage(message);
}

}
fclose(fp);
getch();
searchEmployee();

}

void searchByDesignation(){
clrscr();
echoHead();
int cnt=0,y=10;
char message[100];
char designation[50];
char input[50];
FILE *fp;
gotoxy(3,5);
cprintf("Enter the designation of the employee to Search For:");
gotoxy(3,6);
scanString(input);

clrscr();
echoHead();

fp=fopen("emp.dat","rb+");
rewind(fp);
if(fp==NULL){//if the file cudnt be opened
	showError("Error opening file!");
	}    else{

while(fread(&theEmployee,sizeof(theEmployee),1,fp))
{
getDesignationByID(designation,theEmployee.designationID);
if(!strcmpi(input,designation)){
cnt++;
gotoxy(2,7);
cprintf("ID");
gotoxy(15,7);
cprintf("Full Name");
gotoxy(35,7);
cprintf("Designation");
gotoxy(55,7);
cprintf("Basic Salary");

gotoxy(2,y);
cprintf("%d",theEmployee.id);
gotoxy(15,y);
cprintf("%s",theEmployee.fullName);
gotoxy(35,y);

cprintf("%s",designation);
gotoxy(55,y);
cprintf("%.2f",getSalaryByDesignation(theEmployee.designationID));
y++;

}
}
if(!cnt) showError("No employee exists with the provided ID!");
else{
sprintf(message,"%d",cnt);
strcat(message," Record(s) found ");
showMessage( message);
}

}
fclose(fp);
getch();
searchEmployee();
}

void convertToLower(char *str){
int i;
for (i=0;str[i];i++)
	str[i]= tolower(str[i]);
}

void searchBySalary(){
clrscr();
echoHead();
float min,max,salary;
int cnt=0;
int y=10;
char message[100];
char designation[50];
FILE *fp;
gotoxy(3,5);
cprintf("Enter Minimum Salary:");
gotoxy(3,6);
scanf("%f",&min);
gotoxy(3,7);
cprintf("Enter Maximum Salary:");
gotoxy(3,8);
scanf("%f",&max);

clrscr();
echoHead();

fp=fopen("emp.dat","rb+");
rewind(fp);
if(fp==NULL){//if the file cudnt be opened
	showError("Error opening file");
	}    else{

while(fread(&theEmployee,sizeof(theEmployee),1,fp))
{
salary=getSalaryByDesignation(theEmployee.designationID);
if(salary>=min && salary<=max){
cnt++;

gotoxy(2,7);
cprintf("ID");
gotoxy(15,7);
cprintf("Full Name");
gotoxy(35,7);
cprintf("Designation");
gotoxy(55,7);
cprintf("Basic Salary");

gotoxy(2,y);
cprintf("%d",theEmployee.id);
gotoxy(15,y);
cprintf("%s",theEmployee.fullName);
gotoxy(35,y);
getDesignationByID(designation,theEmployee.designationID);
cprintf("%s",designation);
gotoxy(55,y);
cprintf("%.2f",salary);
y++;
}
}
if(!cnt) showError("No employee exists with the provided ID!");
else{
sprintf(message,"%d",cnt);
strcat(message," Record(s) found ");
showMessage( message);
}

}
fclose(fp);
getch();
searchEmployee();

}




void listEmployees(){

FILE *fp;
long int size= sizeof(theEmployee);
int y=7;
char designation[20];
clrscr();
echoHead();
gotoxy(5,5);
cprintf("ID");

gotoxy(15,5);
cprintf("Full Name");

gotoxy(35,5);
cprintf("Designation");

gotoxy(50,5);
cprintf("Basic Salary");

fp=fopen("emp.dat","rb+");
rewind(fp);
if(fp==NULL){//if the file cudnt be opened
	showError("Error opening file");
	}

	while(fread(&theEmployee,size,1,fp))
	{
	gotoxy(5,y);
	cprintf("%d",theEmployee.id);
	gotoxy(15,y);
	cprintf("%s",theEmployee.fullName);
	gotoxy(35,y);
	getDesignationByID(designation,theEmployee.designationID);
	cprintf("%s",designation);
	gotoxy(50,y);
	cprintf("%.2f",getSalaryByDesignation(theEmployee.designationID));
	y++;
	}

fclose(fp);
getch();
manageEmployees();

}

void searchEmployee(){
clrscr();
char c;
echoHead();
showMessage("Logged in as admin!");

//the box for main menu
makeBox(1,5,40,12);
//the header "Search :"
gotoxy(5,5);
cprintf(" Search: ");
//the menu items:
gotoxy(4,7);
cprintf(" 1. Search by Employee ID");
gotoxy(4,9);
cprintf(" 2. Search by Employee Name");
gotoxy(4,11);
cprintf(" 3. Search by Designation");
gotoxy(4,13);
cprintf(" 4. Search by Salary Range");
gotoxy(4,15);
cprintf(" 5. Back to Employees Menu");
gotoxy(4,17);
cprintf(" 6. Back to Main Menu");
gotoxy(4,19);
cprintf(" 7. EXIT");
cprintf(" Press [1/2/3/4/5/6/7] : ");

do{
fflush(stdin);
c=getch();

switch (c){
	case '1':
	searchByID();
	break;
	case '2':
	searchByName();
	break;
	case '3':
	searchByDesignation();
	break;
	case '4':
	searchBySalary();
	break;
	case '5':
	manageEmployees();
	break;
	case '6':
	displayMainMenu();
	break;
	case '7':
	doExit();
	break;
	default:
	break;
	}
	}
	while (c!='1' || c!='2' || c!='3' || c!='4'|| c!='5' || c!='6' || c!='7');


}

void addEmployee(){
FILE *fp;
long int size = sizeof(theEmployee);
clrscr();
echoHead();
gotoxy(5,7);
cprintf("Employee ID:");
gotoxy(5,9);
cprintf("Full Name:");
gotoxy(5,11);
cprintf("Designation ID:");
_setcursortype(_NORMALCURSOR);

//get and print the autogenerated designation id
theEmployee.id=getNextEmployeeID();
gotoxy(25,7);
cprintf("%d",theEmployee.id);

gotoxy(25,9);
fflush(stdin);
scanString(theEmployee.fullName);

gotoxy(25,11);
fflush(stdin);
scanf("%d",&theEmployee.designationID);

gotoxy(25,13);
fflush(stdin);
//scanString(theEmployee.description);

fp=fopen("emp.dat","ab+");

if (fp==NULL) //if the file couldn't be opened
{
	showError("The file couldn't be opened! Press any key to return!");
}
	else{
	fwrite(&theEmployee,size,1,fp);
	fclose(fp);
	showMessage("Successfully Saved!");
	}
fclose(fp);
	getch();
	manageEmployees();

}

void editEmployee(){
clrscr();
FILE *fp;
long int size = sizeof(theEmployee);
int id;
int flg=0;
fp=fopen("emp.dat","rb+");
echoHead();
_setcursortype(_SOLIDCURSOR);
gotoxy(5,7);
cprintf("Enter the ID of the employee to edit : ");
gotoxy(7,12);
cprintf("Note : You may want to use the search/listing from the previous menu");
gotoxy(14,13);
cprintf("to get/view ID of the employees!");
gotoxy(6,8);
scanf("%d",&id);


clrscr();
echoHead();
rewind(fp);
while(fread(&theEmployee,sizeof(theEmployee),1,fp))
{
if(theEmployee.id==id){
flg=1;//set found flag to true
gotoxy(3,10);
cprintf("Old Value:");
gotoxy(3,16);
cprintf("New Value:");
gotoxy(15,7);
cprintf("ID");
gotoxy(25,7);
cprintf("Full Name");
gotoxy(50,7);
cprintf("Designation ID");
gotoxy(15,10);
cprintf("%d",theEmployee.id);
gotoxy(25,10);
cprintf("%s",theEmployee.fullName);
gotoxy(50,10);
cprintf("%d",theEmployee.designationID);

gotoxy(15,16);
fflush(stdin);
scanf("%d",&theEmployee.id);
gotoxy(25,16);
fflush(stdin);
scanString(theEmployee.fullName);
gotoxy(50,16);
fflush(stdin);
scanf("%d",&theEmployee.designationID);
//seek one record up
fseek(fp,-size,SEEK_CUR);
fwrite(&theEmployee,size,1,fp);
showMessage("Successfully Saved!");
break;
}
}
if(!flg) showError("No employee exists with the provided ID!");
fclose(fp);
getch();
manageEmployees();
}

void removeEmployee(){
clrscr();
FILE *fp, *ft;
int id, flg=0;
char ans;
fp=fopen("emp.dat","rb+");
echoHead();
_setcursortype(_SOLIDCURSOR);
gotoxy(5,7);
cprintf("Enter the ID of the employee to delete : ");
gotoxy(7,12);
cprintf("Note : You may want to use the listing/search from the previous menu");
gotoxy(14,13);
cprintf("to get/view ID of the employees!");
gotoxy(6,8);
scanf("%d",&id);

//Get confirmation from user
gotoxy(10,15);
cprintf("Are you sure you want to remove the employee? (y/n):");
fflush(stdin);
ans=getche();
if(ans=='y' || ans=='Y'){
ft=fopen("tmp.dat","wb+");
rewind(fp);
while(fread(&theEmployee,sizeof(theEmployee),1,fp))
{
if(theEmployee.id!=id){
fwrite(&theEmployee,sizeof(theEmployee),1,ft);
}else flg=1;
}
if(flg){
fclose(fp);
fclose(ft);
remove("emp.dat");
rename("tmp.dat","emp.dat");
showMessage("Employee successfully deleted!");
}else{
showError("Employee not found for the given ID!");
}
}else{
showError("You chose not to remove!");
}
fclose(fp);
fclose(ft);
getch();
manageEmployees();

}

void listDesignations(){

FILE *fp;
long int size= sizeof(theDesignation);
int y=7;
clrscr();
echoHead();



gotoxy(5,5);
cprintf("ID");

gotoxy(15,5);
cprintf("Designation");

gotoxy(35,5);
cprintf("Basic Salary");

gotoxy(52,5);
cprintf("Description");

fp=fopen("des.dat","rb+");
rewind(fp);
if(fp==NULL){//if the file cudnt be opened
	showError("Error opening file");
	}

	while(fread(&theDesignation,size,1,fp))
	{
	gotoxy(5,y);
	cprintf("%d",theDesignation.id);
	gotoxy(15,y);
	cprintf("%s",theDesignation.designation);
	gotoxy(35,y);
	cprintf("%.2f",theDesignation.salary);
	gotoxy(50,y);
	cprintf("%s",theDesignation.description);
	y++;
	}
fclose(fp);
getch();
manageDesignations();
}

void addDesignation(){
FILE *fp;
long int size = sizeof(theDesignation);
clrscr();
echoHead();
gotoxy(5,7);
cprintf("Designation ID:");
gotoxy(5,9);
cprintf("Designation:");
gotoxy(5,11);
cprintf("Basic Salary:");
gotoxy(5,13);
cprintf("Description:");

_setcursortype(_NORMALCURSOR);

//get and print the autogenerated designation id
theDesignation.id=getNextDesignationID();
gotoxy(25,7);
cprintf("%d",theDesignation.id);

gotoxy(25,9);
fflush(stdin);
scanString(theDesignation.designation);

gotoxy(25,11);
fflush(stdin);
scanf("%f",&theDesignation.salary);

gotoxy(25,13);
fflush(stdin);
scanString(theDesignation.description);

fp=fopen("des.dat","ab+");

if (fp==NULL) //if the file couldn't be opened
{
	showError("The file couldn't be opened! Press any key to return!");
}
	else{
	fwrite(&theDesignation,size,1,fp);
	fclose(fp);
	showMessage("Successfully Saved!");
	}
fclose(fp);
	getch();
	manageDesignations();

}

void editDesignation(){
clrscr();
FILE *fp;
long int size = sizeof(theDesignation);
int id;
int flg=0;
fp=fopen("des.dat","rb+");
echoHead();
_setcursortype(_SOLIDCURSOR);
gotoxy(5,7);
cprintf("Enter the ID of the designation to edit : ");
gotoxy(7,12);
cprintf("Note : You may want to use the listing from the previous menu");
gotoxy(14,13);
cprintf("to get/view ID of all designations!");
gotoxy(6,8);
scanf("%d",&id);


clrscr();
echoHead();
rewind(fp);
while(fread(&theDesignation,sizeof(theDesignation),1,fp))
{
if(theDesignation.id==id){
flg=1;//set found flag to true
gotoxy(3,10);
cprintf("Old Value:");
gotoxy(3,16);
cprintf("New Value:");
gotoxy(15,7);
cprintf("ID");
gotoxy(25,7);
cprintf("Designation");
gotoxy(40,7);
cprintf("Basic Salary");
gotoxy(50,7);
cprintf("Description");
gotoxy(15,10);
cprintf("%d",theDesignation.id);
gotoxy(25,10);
cprintf("%s",theDesignation.designation);
gotoxy(40,10);
cprintf("%.2f",theDesignation.salary);
gotoxy(50,10);
cprintf("%s",theDesignation.description);

gotoxy(15,16);
fflush(stdin);
scanf("%d",&theDesignation.id);
gotoxy(25,16);
fflush(stdin);
scanString(theDesignation.designation);
gotoxy(40,16);
fflush(stdin);
scanf("%f",&theDesignation.salary);
gotoxy(50,16);
fflush(stdin);
scanString(theDesignation.description);
//seek one record up
fseek(fp,-size,SEEK_CUR);
fwrite(&theDesignation,size,1,fp);
break;
}
}
if(flg){
showMessage("Designation successfully edited and saved!");
}
else{
showMessage("No designations found with given ID!");
}

fclose(fp);
getch();
manageDesignations();


}

void removeDesignation(){
clrscr();
FILE *fp, *ft;
int id, flg=0;
char ans;
fp=fopen("des.dat","rb+");
echoHead();
_setcursortype(_SOLIDCURSOR);
gotoxy(5,7);
cprintf("Enter the ID of the designation to delete : ");
gotoxy(7,12);
cprintf("Note : You may want to use the listing from the previous menu");
gotoxy(14,13);
cprintf("to get/view ID of all designations!");
gotoxy(6,8);
scanf("%d",&id);

//Get confirmation from user
gotoxy(10,15);
cprintf("Are you sure you want to remove the designation? (y/n):");
fflush(stdin);
ans=getche();
if(ans=='y' || ans=='Y'){
ft=fopen("tmp.dat","wb+");
rewind(fp);
while(fread(&theDesignation,sizeof(theDesignation),1,fp))
{
if(theDesignation.id!=id){
fwrite(&theDesignation,sizeof(theDesignation),1,ft);
}else flg=1;
}
if(flg){
remove("des.dat");
rename("tmp.dat","des.dat");
showMessage("Designation successfully deleted!");
}else{
showError("No Designations exist with the given ID!");
}
}else{
showError("You chose not to remove!");
}
fclose(fp);
fclose(ft);
getch();
manageDesignations();

}



void manageEmployees(){
clrscr();
char c;
echoHead();
//the box for main menu
makeBox(1,5,40,12);
//the header "Menu :"
gotoxy(5,5);
cprintf(" Menu: ");
//the menu items:
gotoxy(4,7);
cprintf(" 1. List All Employees");
gotoxy(4,9);
cprintf(" 2. Search An Employee");
gotoxy(4,11);
cprintf(" 3. Add an Employee");
gotoxy(4,13);
cprintf(" 4. Edit an Empoyee's Details");
gotoxy(4,15);
cprintf(" 5. Remove an Employee");
gotoxy(4,17);
cprintf(" 6. Back to Main Menu");
gotoxy(4,19);
cprintf(" 7. EXIT!");
gotoxy(4,21);
cprintf(" Press [1/2/3/4/5/6/7] : ");

do{
fflush(stdin);
c=getch();



switch (c){
	case '1':
	listEmployees();
	break;
	case '2':
	searchEmployee();
	break;
	case '3':
	addEmployee();
	break;
	case '4':
	editEmployee();
	break;
	case '5':
	removeEmployee();
	break;
	case '6':
	displayMainMenu();
	break;
	case '7':
	doExit();
	break;
	default:
	break;
	}
	} while (c!='1' || c!='2' || c!='3' || c!='4' || c!='5' || c!='6' || c!='7');


}

void manageDesignations(){
clrscr();
char c;
echoHead();
//the box for main menu
makeBox(1,5,40,12);
//the header "Menu :"
gotoxy(5,5);
cprintf(" Menu: ");
//the menu items:
gotoxy(4,7);
cprintf(" 1. List All Designations");
gotoxy(4,9);
cprintf(" 2. Add a Designation");
gotoxy(4,11);
cprintf(" 3. Edit a Designation");
gotoxy(4,13);
cprintf(" 4. Remove a Designation");
gotoxy(4,15);
cprintf(" 5. Back to Main Menu");
gotoxy(4,17);
cprintf(" 6. EXIT!");
gotoxy(4,19);
cprintf(" Press [1/2/3/4/5/6] : ");

do{
fflush(stdin);
c=getch();



switch (c){
	case '1':
	listDesignations();
	break;
	case '2':
	addDesignation();
	break;
	case '3':
	editDesignation();
	break;
	case '4':
	removeDesignation();
	break;
	case '5':
	displayMainMenu();
	break;
	case '6':
	doExit();
	break;
	default:
	break;
	}
	} while (c!='1' || c!='2' || c!='3' || c!='4' || c!='5' || c!='6' );

}

void generateReport(){
clrscr();
echoHead();

//file pointers for employee and designation data file
FILE *fpe,*fpd;

//create and initialize counter variables;
int employees=0, designations=0;
//and variables for sum amount
float salary=0, tax=0, fund=0;
//a temporary variable for storing retrieved salary
float tmp;

//open those files for reading
fpe=fopen("emp.dat","r");
fpd=fopen("des.dat","r");

while(fread(&theEmployee,sizeof(theEmployee),1,fpe)){
//get the salary of each employee in the tmp variable
tmp=getSalaryByDesignation(theEmployee.designationID);
//keep adding it to the total salary
salary+=tmp;
//add 10% of it to the total fund
fund+=(0.1*tmp);
//and reduce the amount submitted in the fund
tmp-=(0.1*tmp);
//tax at rate of 15%
tax+=(0.15*tmp);
//deduce the tax amount
tmp-=(0.15*tmp);
employees++;
}

while(fread(&theDesignation,sizeof(theDesignation),1,fpd)){
designations++;
}

gotoxy(3,5);
cprintf("Number of Employees : %d",employees);
gotoxy(3,7);
cprintf("Number of Designations : %d",designations);
gotoxy(3,9);
cprintf("Total Basic Salary : %.2f",salary);
gotoxy(3,11);
cprintf("Total Amount deposited in Provident Fund : %.2f",fund);
gotoxy(3,13);
cprintf("Total Tax Amount : %.2f",tax);
gotoxy(3,15);
cprintf("Net Salary to be Distributed : %.2f",salary-tax-fund);

showMessage("Report Successfully generated!");







getch();
displayMainMenu();

}

void doExit(){
clrscr();
echoHead();
textcolor(RED);
_setcursortype(_NORMALCURSOR);
gotoxy(2,7);
cprintf("Thank you for using NepZilla Payroll System!");
gotoxy(2,8);
cprintf("Press any key to exit now!");
getch();
exit(0);
}



void displayLogin(){
clrscr();
char username[50];
char password[50];
int c=0;
echoHead();
//the login box
makeBox(3,7,40,10);
//the header "Menu :"
gotoxy(7,7);
cprintf(" Login: ");
//the input fields
gotoxy(6,9);
cprintf(" Username: ");
gotoxy(6,13);
cprintf(" Password: ");
//boxes for username and password input
makeBox(19,8,15,2);
makeBox(19,12,15,2);

showMessage("Ta-dang! Authentication Please!!!");

do{
textcolor(GREEN);
gotoxy(20,9);
//clear the existsing username
cprintf("             ");
gotoxy(20,9);
//get the username
fflush(stdin);
scanString(username);
gotoxy(20,13);
cprintf("              ");
gotoxy(20,13);
scanPassword(password);

if(strcmp(username,"admin")){
showError("Invalid user!");
c++;
}
else if (strcmp(password,"passweird")){
showError("Incorrect  Password!");
c++;
}
else{
textcolor(GREEN);
displayMainMenu();
break;
}
}while(c<3);
if(c==3)
showError("Maximum attempts exceeded! Press any key to exit!");
}

void showMessage(char *message){
_setcursortype(_NORMALCURSOR);
int i;
//make the box
makeBox(1,22,79,2);
gotoxy(2,23);
//clear old messages
for ( i=2;i<79;i++){
cprintf(" ");}
gotoxy(2,23);
cprintf("%s",message);
}

void showError(char *error){
textcolor(RED+BLINK);
showMessage(error);
}

void displayMainMenu(){
clrscr();
char c;
echoHead();
showMessage("Logged in as admin!");

//the box for main menu
makeBox(1,5,40,12);
//the header "Menu :"
gotoxy(5,5);
cprintf(" Menu: ");
//the menu items:
gotoxy(4,7);
cprintf(" 1. View/manage Employees");
gotoxy(4,9);
cprintf(" 2. View/manage Designations");
gotoxy(4,11);
cprintf(" 3. Generate Report");
gotoxy(4,13);
cprintf(" 4. EXIT");

gotoxy(4,15);
cprintf(" Press [1/2/3/4] : ");

do{
fflush(stdin);
c=getch();

switch (c){
	case '1':
	manageEmployees();
	break;
	case '2':
	manageDesignations();
	break;
	case '3':
	generateReport();
	break;
	case '4':
	doExit();
	break;
	default:
	break;
	}
	}
	while (c!='1' || c!='2' || c!='3' || c!='4');


}

void echoHead(){
textcolor(YELLOW-1);
textbackground(MAGENTA);
_setcursortype(_SOLIDCURSOR);
clrscr();
makeBox(1,2,79,2);
gotoxy(28,3);
cprintf("NepZilla Payroll System");
textcolor(YELLOW);
}


void makeBox(int x, int y, int width, int height)
{
	int i,j;
	//top left corner
	gotoxy(x,y);
	cprintf("%c",218);

	//top right corner
	gotoxy(x+width,y);
	cprintf("%c",191);

	//bottom left corner
	gotoxy(x,y+height);
	cprintf("%c",192);

	//bottom right corner
	gotoxy(x+width, y+height);
	cprintf("%c",217);

	for(i=x+1;i<x+width;i++)
	{
		//for top line

		gotoxy(i,y);
		cprintf("%c",196);

		//for bottom line
		gotoxy(i,y+height);
		cprintf("%c",196);
	}

	for(j=y+1;j<y+height;j++)
	{
		//for left line
		gotoxy(x,j);
		cprintf("%c",179);

		//for right line
		gotoxy(x+width, j);
		cprintf("%c",179);
	}
}

void splashScreen(){//the first screen of the program with information
	_setcursortype(_NOCURSOR);
	textbackground(WHITE);
	clrscr();
	textcolor(GREEN);
//	printArt();
	int x=35,y=5;
	makeBox(x,y,36,5);
	gotoxy(x+1,y+1);
	cprintf("Name : Dipesh Acharya");
	gotoxy(x+1,y+2);
	cprintf("Student ID : 1801T2100053");
	gotoxy(x+1,y+3);
	cprintf("Module : CPG102");
	gotoxy(x+1,y+4);
	cprintf("Submitted To : Mr. Prakash Shrestha");

	echoContinue();

//	makeBox(18,8,45,4);
       }

       void echoContinue(){
	int x=2,y=22;
	makeBox(x,y,40,2);
	gotoxy(x+1,y+1);
	cprintf("Press any key to Continue......");
	getch();
	}


void printArt(){
gotoxy(18,5);
cprintf(" _   _             ______ _ _ _       ");
gotoxy(18,6);
cprintf("| \ | |           |___  /(_) | |        ");
cprintf("\n");
gotoxy(18,7);
cprintf("|  \| | ___ _ __     / /  _| | | __ _   ");
cprintf("\n");
gotoxy(18,8);
cprintf("| . ` |/ _ \ '_ \   / /  | | | |/ _` |  ");
cprintf("\n");
gotoxy(18,9);
cprintf("| |\  |  __/ |_) |./ /___| | | | (_| |  ");
cprintf("\n");
gotoxy(18,10);
cprintf("\_| \_/\___| .__/ \_____/|_|_|_|\__,_|  ");
cprintf("\n");
gotoxy(18,11);
cprintf("	   | |                           ");
cprintf("\n");
gotoxy(18,12);
cprintf("	   |_|                           ");
gotoxy(18,13);
cprintf("\n");
}

void scanPassword(char *in)
{
	char inputStr[50];
	char input;
	int i=0;
	_setcursortype(_SOLIDCURSOR);
	do{
		input=getch();
	//if tab, carriage return or esc is pressed
		if((input==9 || input==11 || input==13 || input==27) && i>0)
		{
			break;
		}
		else if(input==8 && i>0) //if backspace button is pressed
		{
			cprintf("%c%c%c",8,32,8);
			i--;
		}
		else
		{
			cprintf("*");
			inputStr[i]=input;
			i++;
		}
	}while(1);

	inputStr[i]='\0';

	strcpy(in,inputStr);
}


void scanString(char *in)
{
	_setcursortype(_SOLIDCURSOR);
	char inputStr[50];
	char input;
	int i=0;

	do{
		input=getch();
		//if tab, carriage return or esc is pressed
		if((input==9 || input==11 || input==13 || input==27) && i>0)
		{
			break;
		}
	else if(input==8 && i>0) //if backspace button is pressed
		{
			cprintf("%c%c%c",8,32,8);
			i--;
		}
		else if(input==32 || (input>47 && input<58) || (input>64 && input<91) || (input>96 && input<123))
		{
			cprintf("%c",input);
			inputStr[i]=input;
			i++;
		}
	}while(1);

	inputStr[i]='\0';

	strcpy(in,inputStr);
}


int scanInt()
{
	char inputStr[50];
	char input;
	int i=0,returnvalue;


	do{
		input=getch();

		//if tab, carriage return or esc is pressed
		if((input==9 || input==11 || input==13 || input==27) && i>0)
		{
			break;
		}

		else if(input==8 && i>0) //if backspace button is pressed
		{
			cprintf("%c%c%c",8,32,8);
			i--;
		}
		else if(input!=8 && (input>=48 && input<=57)) //only accept digits 0-9
		{
			cprintf("%c",input);
			inputStr[i]=input;
			i++;
		}
	}while(i<=4);

	inputStr[i]='\0';
	return atoi(inputStr);

}
